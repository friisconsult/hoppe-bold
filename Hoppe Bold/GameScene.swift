//
//  GameScene.swift
//  Hoppe Bold
//
//  Created by Per Friis on 22/03/16.
//  Copyright (c) 2016 Mathis. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    let bold = SKSpriteNode(imageNamed: "bold")
    let pad = SKSpriteNode(color: UIColor.blueColor(), size: CGSize(width: 128, height: 58))
    
    
    override func didMoveToView(view: SKView) {
        
        physicsBody = SKPhysicsBody(edgeLoopFromRect:frame)
     
        bold.position = CGPoint(x: frame.midX, y: frame.midY * 1.5)
        bold.xScale = 0.25
        bold.yScale = 0.25
        addChild(bold)
        
        bold.physicsBody = SKPhysicsBody(circleOfRadius: 64)
        bold.physicsBody?.mass = 1
        bold.physicsBody?.linearDamping = 0
        bold.physicsBody?.restitution = 1.02
        
        bold.physicsBody?.applyTorque(25)
        
        pad.position = CGPoint(x: frame.midX, y: 58)
        pad.physicsBody = SKPhysicsBody(rectangleOfSize: pad.size)
        pad.physicsBody?.affectedByGravity = false
        pad.physicsBody?.dynamic = false
        
        pad.physicsBody?.restitution = 1
        
        addChild(pad)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            let padX = pad.position.x
            let flyt : SKAction
            
            if location.x > padX {
                let afstand = location.x - padX
                let duration = 1 * (afstand / padX)
                flyt = SKAction.moveToX(location.x, duration: Double(duration))
                
            } else {
                let afstand = padX - location.x
                let duration = 1 * (afstand / padX)
                flyt = SKAction.moveToX(location.x, duration: Double(duration))
            }
            
            pad.runAction(flyt)
            
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
